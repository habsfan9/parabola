/*
 Create polygon p
 Overwrite paint component
 
 Implement loop to add points to p
 Draw line through the points
 
 Draw the axes around the parabola
 Label axes
 
 Create frame and set size, layout, visibility, etc.
 
 */

import java.awt.*;
import javax.swing.*;

public class Parabola extends JFrame 
{
	  public static class GraphPanel extends JPanel
	  {
		  public Polygon p;
	  
		  protected void paintComponent(Graphics g) 
		  {
			  p = new Polygon();
		  
			  double scaleFactor = 0.1;
			  for (int x=-100; x<=100; x++) 
			  {
				  p.addPoint(x+200, 200- (int)(scaleFactor *x *x));
			  }
			  g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
			  
			  int xAxisx1 = 100;
			  int xAxisx2 = 300;
			  int xAxisy1 = 220;
			  int xAxisy2 = 220;
			  g.drawLine(xAxisx1, xAxisy1, xAxisx2, xAxisy2);
			  g.drawString("X Axis", (((xAxisx1 + xAxisx2 )/ 2) - 20), (xAxisy1 + 20));
			  
			  int yAxisx1 = 100;
			  int yAxisx2 = 100;
			  int yAxisy1 = 0;
			  int yAxisy2 = 220;
			  g.drawLine(yAxisx1, yAxisy1, yAxisx2, yAxisy2);
			  g.drawString("Y Axis", (yAxisx1 - 50), ((yAxisy1 + yAxisy2 )/ 2));
			  
			  g.drawString("Parabola", (((xAxisx1 + xAxisx2 )/ 2) - 25), (xAxisy1 + 100));
		  }
	  }
	  
	  public static void main(String[] args) 
	  {
	    JFrame frame = new JFrame();
	    frame.getContentPane().add(new GraphPanel());

	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(400,400);
	    frame.setVisible(true);
	    frame.setTitle("Parabola");
	    frame.setLayout(new FlowLayout());
	  }
}